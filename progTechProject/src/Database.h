/*
 * Database.h
 *
 *  Created on: Feb 12, 2019
 *      Author: madtracer
 */

#ifndef DATABASE_H_
#define DATABASE_H_

#include <fstream>
#include <cstdint>
#include <string>
#include <vector>
#include "User.h"
#include "Comment.h"
#include "Post.h"
#include "Message.h"

using namespace std;

class Database {
public:
	Database();
	Database(string url);
	virtual ~Database();

//--------------------------------------------READING METHODS------------------------------------------------
	//------Users------
//	static User* userAuthentication(std::string sLogin, std::string sPassword);
//	static User* getUserByLogin(std::string sLogin);
	vector<User*> getAllUsers();
	//------Comments------
	vector<Comment*> getCommentsByPostId(int sPostId);
	//------Posts------
//	Post* getPostById(int sId);
	vector<Post*> getPostsByCreator(std::string sCreator);
	vector<Post*> getPostsByDate(std::string sDate);
	//------Messages------
	vector<Message*> getDialog(std::string sSender, std::string sRecipient);
//--------------------------------------------WRITING METHODS------------------------------------------------
	//------Users------
	bool pushUsers(vector<User*> pUsers);
	//------Comments------
	bool pushComments(vector<Comment*> pComments);
	//------Posts------
	bool pushPosts(vector<Post*> pPost);
	//------Messages------
	bool pushMessages(vector<Message*> pMessages);
private:
	ifstream _fin;
	ofstream _fout;
	string _url;
protected:

};

#endif /* DATABASE_H_ */
