/*
 * Message.cpp
 *
 *  Created on: Mar 8, 2019
 *      Author: madtracer
 */

#include "Message.h"

Message::Message() {
	// TODO Auto-generated constructor stub

}

Message::Message(int id, std::string sender, std::string recipient, std::string text) {
	_id = id;
	_sender = sender;
	_recipient = recipient;
	_text = text;
}

Message::~Message() {
	// TODO Auto-generated destructor stub
}

//------Setters------
bool Message::setMessageId(int id) {
	_id = id;
	return true;
}

bool Message::setSender(std::string sender) {
	_sender = sender;
	return true;
}

bool Message::setRecipient(std::string recipient) {
	_recipient = recipient;
	return true;
}

bool Message::setText(std::string text) {
	_text = text;
	return true;
}

//------Getters------
int Message::getMessageId() {
	return _id;
}

std::string Message::getSender() {
	return _sender;
}

std::string Message::getRecipient() {
	return _recipient;
}

std::string Message::getText() {
	return _text;
}
