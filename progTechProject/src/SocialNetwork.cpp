/*
 * SocialNetwork.cpp
 *
 *  Created on: Jan 29, 2019
 *      Author: madtracer
 */

#include "SocialNetwork.h"

SocialNetwork::SocialNetwork(){
	_name = "";
	_address = "";
	_lUser = NULL;
	db = NULL;
}

SocialNetwork::~SocialNetwork() {}

//------ Setters ------

bool SocialNetwork::setName(std::string name) {
	for (int i = 0; i < name.length(); i++) {
		if (!isalpha(name[i])){
			return false;
		}
	}
	_name = name;
	return true;
}

bool SocialNetwork::setAddress(std::string address){
	for (int i = 0; i < address.length(); i++){
		if (address[i] >= 'A' && address[i] <= 'Z')
			address[i] += 0x20;
	}
	_address = address;
	return true;
}

//------Actions------

bool SocialNetwork::initSocialNetwork(std::string url) {
	db = new Database(url);
	SocialNetwork::_users = db->getAllUsers();
	SocialNetwork::setAddress(url);
	if (url == "vk.com"){
		SocialNetwork::setName("VK");
	}
	else if (url == "instagram.com"){
		SocialNetwork::setName("Instagram");
	}
	else {
		cout << "Incorrect URL..." << endl;
		return false;
	}
	return true;
}

bool SocialNetwork::authentication(string login, string password) {
	for (int i = 0; i < _users.size(); i++){
		if (_users[i]->getLogin() == login && _users[i]->getPassword() == password){
			_lUser = new User;
			_lUser = _users[i];
			return true;
		}
	}
	return false;
}

bool SocialNetwork::exitSn() {
	db->pushUsers(_users);
	db->pushPosts(_posts);
	db->pushComments(_comments);
	db->pushMessages(_messages);
	exit(0);
	return true;
}

//------ Getters ------

std::string SocialNetwork::getName(){
	return _name;
}

std::string SocialNetwork::getAddress(){
	return _address;
}

