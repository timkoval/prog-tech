/*
 * Browser.h
 *
 *  Created on: Feb 19, 2019
 *      Author: madtracer
 */

#ifndef BROWSER_H_
#define BROWSER_H_

#include "SocialNetwork.h"
#include "Vk.h"
#include "Instagram.h"

class Browser {
public:
	Browser();
	Browser(std::string url);
	~Browser();

	void run();

private:
	SocialNetwork *sn;
};

#endif /* BROWSER_H_ */
