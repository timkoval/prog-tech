/*
 * Comment.cpp
 *
 *  Created on: Jan 30, 2019
 *      Author: madtracer
 */

#include "Comment.h"

Comment::Comment() {
	_postId = 0;
	_text = "";
	_creator = "";
}

Comment::Comment(int id, int parentPostId, std::string creator, std::string text) {
	setPostId(id);
	setCurDate();
	setParentPostId(parentPostId);
	setCreator(creator);
	setText(text);
}

Comment::~Comment() {
}

bool Comment::setParentPostId(int parentPostId){
	_parentPostId = parentPostId;
	return true;
}

int Comment::getParentPostId(){
	return _parentPostId;
}
