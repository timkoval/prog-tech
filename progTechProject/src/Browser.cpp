/*
 * Browser.cpp
 *
 *  Created on: Feb 19, 2019
 *      Author: madtracer
 */

#include "Browser.h"

Browser::Browser() {

}

Browser::Browser(std::string url){
	if (url == "vk.com")
		sn = new Vk;
	else if (url == "instagram.com")
		sn = new Instagram;

	if (sn->initSocialNetwork(url))
		Browser::run();
	else
		exit(0);
}

Browser::~Browser() {
	// TODO Auto-generated destructor stub
}

void Browser::run() {
		sn->showMainMenu();
}
