/*
 * Vk.h
 *
 *  Created on: Mar 8, 2019
 *      Author: madtracer
 */

#ifndef VK_H_
#define VK_H_

#include "SocialNetwork.h"

class Vk : public SocialNetwork {
public:
	Vk();
	virtual ~Vk();

	void showMainMenu();
	void showUserMenu();
	void showPostMenu();
	void showFriendMenu();
	void showMessageMenu();

};

#endif /* VK_H_ */
