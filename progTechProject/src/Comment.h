/*
 * Comment.h
 *
 *  Created on: Jan 30, 2019
 *      Author: madtracer
 */

#ifndef COMMENT_H_
#define COMMENT_H_

#include "Post.h"

class Comment : public Post {
public:
	Comment();
	Comment(int id, int parentPostId, std::string creator, std::string text);
	virtual ~Comment();

	bool setParentPostId(int parentPostId);
	int getParentPostId();

private:
	int _parentPostId;
protected:
};

#endif /* COMMENT_H_ */
