/*
 * Vk.cpp
 *
 *  Created on: Mar 8, 2019
 *      Author: madtracer
 */

#include "Vk.h"

Vk::Vk():SocialNetwork() {
	// TODO Auto-generated constructor stub

}

Vk::~Vk() {
	// TODO Auto-generated destructor stub
}

//------Actions-----
void Vk::showMainMenu(){
	char action;
	cout << "--------MENU--------" << endl;
	cout << "1 - Log In" << endl;
	cout << "2 - Sign up" << endl;
	cout << "3 - Exit" << endl;
	cin >> action;
	switch (action){
	case '1':
	{
		bool state;
		string login, password;
		cout << "Enter your login and password : " << endl;
		cin >> login >> password;
		state = SocialNetwork::authentication(login, password);
		if (state == 1) {
			cout << "Welcome " << login << " !!!" << endl;
			_posts = db->getPostsByCreator(login);
			Vk::showUserMenu();
		}
		else {
			cout << "Incorrect login or password!" << endl;
			Vk::showMainMenu();
		}
		}
		break;
	case '2':
	{
		int id;
		string login, password, role, username;
		User *curUser;
		User *lastUser = _users.back();
		id = lastUser->getUserId() + 1;

		cout << "Login : ";
		cin >> login;
		cout << "Password : ";
		cin >> password;
		cout << "Username : ";
		cin >> username;
		cout << "Role : ";
		cin >> role;

		curUser = new User(id, login, password, role, username);
		_users.push_back(curUser);
		Vk::showMainMenu();
	}
	break;
	case '3':
	{
		cout << "Good bye!";
		exit(0);
	}
		break;
	}
}


void Vk::showUserMenu(){
	char userAction;
	string creator, text;

	cout << "1 - Show feed : " << endl;
	cout << "2 - Create post :" << endl;
	cout << "3 - Show post : " << endl;
	cout << "4 - Delete post : " << endl;
	cout << "5 - See friends :" << endl;
	cout << "6 - Exit" << endl;
	cin >> userAction;

	switch (userAction){
	case '1':
	{
		for (int i = 0; i < _posts.size(); i++){
				cout << _posts[i]->getPostId() << " " << _posts[i]->getDate() << " " << _posts[i]->getCreator() << endl << _posts[i]->getText() << endl;
		}
		Vk::showUserMenu();
	}
	break;
	case '2':
	{
		int id;
		string creator, imgPath, text;
		Post *curPost;
		Post *lastPost = _posts.back();
		id = lastPost->getPostId() + 1;

		cout << "Enter image path : " << endl;
		cin >> imgPath;
		cout << "Write your post : " << endl;
		cin.ignore();
		getline(cin, text);

		curPost = new Post(id, _lUser->getLogin(), imgPath, text);
		_posts.push_back(curPost);
		Vk::showUserMenu();
	}
		break;
	case '3':
	{
		int id;
		cout << "Enter post ID :" << endl;
		cin >> id;
		for (int i = 0; i < _posts.size(); i++){
			if (_posts[i]->getPostId() == id){
				cout << _posts[i]->getDate() << " " << _posts[i]->getCreator() << endl << _posts[i]->getText() << endl;
				_lPost = _posts[i];
			}
		}
		_comments = db->getCommentsByPostId(_lPost->getPostId());
		Vk::showPostMenu();
	}
		break;
	case '4':
	{
		int id;
		cout << "Enter post ID:" << endl;
		cin >> id;
		for (int i = 0; i < _posts.size(); i++){
			if (_posts[i]->getPostId() == id){
				if (_posts[i]->getCreator() == _lUser->getLogin())
				_posts[i] = NULL;
			}
		}
	}
	break;
	case '5':
	{
		_friends = _lUser->getFriends();
		for (int i = 0; i < _friends.size(); i++){
			for (int j = 0; j < _users.size(); j++){
				if (_friends[i] == _users[j]->getUserId())
					cout << _users[j]->getUserId() << " " << _users[j]->getLogin() << " " << _users[j]->getUsername() << endl;
			}
		}
		Vk::showFriendMenu();
	}
	break;
	case '6':
		cout << "Good bye!";
		exit(0);
		break;
	}
}

void Vk::showPostMenu(){
	char postAction;

	cout << "1 - Show comments : " << endl;
	cout << "2 - Write comment : " << endl;
	cout << "3 - Show comment by ID : " << endl;
	cout << "4 - Like : " << endl;
	cin >> postAction;

	switch (postAction) {
	case '1':
	{
		for (int i = 0; i < _comments.size(); i++){
			cout << _comments[i]->getPostId() << " " << _comments[i]->getDate() << " " << _posts[i]->getCreator() << endl << _posts[i]->getText() << endl;
		}
	}
	break;
	case '2':
	{
		int id;
		string commentCreator, text;
		Comment *curComment;
		Comment *lastComment = _comments.back();
		id = lastComment->getPostId() + 1;

		cout << "Write your comment : " << endl;
		cin.ignore();
		getline(cin, text);

		curComment = new Comment(id, _lPost->getPostId(), _lUser->getLogin(), text);
		_posts.push_back(curComment);
		Vk::showPostMenu();
		}
		break;
	case '3':
	{
		int id;
		cout << "Enter comment ID :" << endl;
		cin >> id;
		for (int i = 0; i < _comments.size(); i++){
			if (_comments[i]->getPostId() == id){
				cout << _comments[i]->getDate() << " " << _posts[i]->getCreator() << endl << _posts[i]->getText() << endl;
			}
		}
	}
	break;
	case '4':
	{
		int likes;
		likes = _lPost->getLikes();
		likes++;
		_lPost->setLikes(likes);
	}
	break;
	}


}

void Vk::showFriendMenu() {
	int friendId;
	cout << "Enter friend's ID :";
	cin >> friendId;
	for (int i = 0; i < _users.size(); i++)
		if (_users[i]->getUserId() == friendId){
			_lFriend = _users[i];
		}

	char action;
	cout << "1 - Show feed : " << endl;
	cout << "2 - Show post : " << endl;
	cout << "3 - Private messages :" << endl;
	cin >> action;
	switch (action){
	case '1':
	{
		for (int i = 0; i < _posts.size(); i++){
			if (_posts[i]->getCreator() == _lFriend->getLogin())
				cout << _posts[i]->getPostId() << " " << _posts[i]->getDate() << " " << _posts[i]->getCreator() << endl << _posts[i]->getText() << endl;
		}
		Vk::showFriendMenu();
	}
	break;
	case '2':
	{
		int id;
		cout << "Enter post ID : " << endl;
		cin >> id;
		for (int i = 0; i < _posts.size(); i++){
			if (_posts[i]->getPostId() == id){
				cout << _posts[i]->getDate() << " " << _posts[i]->getCreator() << endl << _posts[i]->getText() << endl;
				_lPost = _posts[i];
			}
		}
		_comments = db->getCommentsByPostId(_lPost->getPostId());
		Vk::showPostMenu();
	}
	break;
	case '3':
	{
		_messages = db->getDialog(_lUser->getLogin(), _lFriend->getLogin());
		Vk::showMessageMenu();
	}
	break;
	}
}

void Vk::showMessageMenu() {
	char action;
	cout << "1 - See dialog : " << endl;
	cout << "2 - Write message :" << endl;
	cin >> action;
	switch(action) {
	case '1':
	{
		for (int i = 0; i < _messages.size(); i++){
			cout << _messages[i]->getText() << endl;
		}
		Vk::showMessageMenu();
	}
	break;
	case '2':
	{
		int id;
		std::string text;
		Message *curMessage;
		Message *lastMessage = _messages.back();
		id = lastMessage->getMessageId() + 1;

		cout << "Write your message : " << endl;
		cin.ignore();
		getline(cin, text);

		curMessage = new Message(id, _lUser->getLogin(), _lFriend->getLogin(), text);
		_messages.push_back(curMessage);
		Vk::showMessageMenu();
	}
	break;
	}
}
