/*
 * Instagram.h
 *
 *  Created on: Mar 8, 2019
 *      Author: madtracer
 */

#ifndef INSTAGRAM_H_
#define INSTAGRAM_H_

#include "SocialNetwork.h"

class Instagram : public SocialNetwork {
public:
	Instagram();
	virtual ~Instagram();

	void showMainMenu();
	void showUserMenu();
	void showPostMenu();
	void showFriendMenu();
	void showMessageMenu();
};

#endif /* INSTAGRAM_H_ */
