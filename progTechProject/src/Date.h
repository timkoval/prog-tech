/*
 * Date.h
 *
 *  Created on: Jan 30, 2019
 *      Author: madtracer
 */

#ifndef DATE_H_
#define DATE_H_

#include <string>
#include <iostream>
#include <cctype>
#include <boost/date_time/gregorian/gregorian.hpp>

class Date {
public:
	Date();
	virtual ~Date();

	bool setDate(std::string dateStr);
	bool setCurDate();

	std::string getDate();

private:
	boost::gregorian::date _dateObj;

protected:

};

#endif /* DATE_H_ */
