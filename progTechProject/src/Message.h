/*
 * Message.h
 *
 *  Created on: Mar 8, 2019
 *      Author: madtracer
 */

#ifndef MESSAGE_H_
#define MESSAGE_H_

#include <iostream>
#include <string>

class Message {
public:
	Message(int id, std::string sender, std::string recipient, std::string text);
	Message();
	virtual ~Message();
	//------Setters------
	bool setMessageId(int id);
	bool setSender(std::string sender);
	bool setRecipient(std::string recipient);
	bool setText(std::string text);
	//------Getters------
	int getMessageId();
	std::string getSender();
	std::string getRecipient();
	std::string getText();

private:
	int _id;
	std::string _sender;
	std::string _recipient;
	std::string _text;
};

#endif /* MESSAGE_H_ */
