/*
 * Date.cpp
 *
 *  Created on: Jan 30, 2019
 *      Author: madtracer
 */

#include "Date.h"

Date::Date() {

}

Date::~Date() {

}

//------ Setters ------

bool Date::setDate(std::string dateStr) {
	_dateObj = boost::gregorian::from_simple_string(dateStr);
	return true;
}

bool Date::setCurDate(){
	_dateObj = boost::gregorian::day_clock::universal_day();
	return true;
}

//------ Getters ------

std::string Date::getDate() {
	return boost::gregorian::to_simple_string(_dateObj);
}

